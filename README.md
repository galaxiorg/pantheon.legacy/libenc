# libenc
libenc is a set of libraries for ComputerCraft, by various people.
-----
### Contributing
Make a pull request with your own implementation and it will be reviewed

### License
There is no license for this repo. However, this doesn't mean they are free to
use. Check the conditions in the header of each file or ask the responsible user.

### Bugs
Contact [me](mailto:daelvn@gmail.com) or the mantainer of the file.

### Notes
This repository is unfinished and some files need changes to work completely in
ComputerCraft.

### Credits
[SquidDev](https://github.com/SquidDev) for aes.lua and aes.min.lua

[Anavrins](https://github.com/xAnavrins) for blake-256.lua, sha1.lua, sha256.lua and md5.lua

[Alex Kloss](mailto://alexthkloss@web.de) for base64.lua

[1lann](https://github.com/1lann) for rsa.lua
