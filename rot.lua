-- Rot47 library, code from Thomas Farr <farr.thomas@gmail.com>
-- License stored in licenses/rot.license
local librot = {}
function librot.rot47(str)  --Rot47s @str
	if not str then return nil end
	str = tostring(str)
	local rot = ""
	for i = 1, str:len() do
		local p = str:byte(i)
		if p >= string.byte('!') and p <= string.byte('O') then
			p = ((p + 47) % 127)
		elseif p >= string.byte('P') and p <= string.byte('~') then
			p = ((p - 47) % 127)
		end
		rot = rot..string.char(p)
	end
	return rot
end
function librot.rot13(str)  --Rot13s @str
	if not str then return nil end
	str = tostring(str)
	local rot = ""
	local len = str:len()
	for i = 1, len do
		local k = str:byte(i)
		if (k >= 65 and k <= 77) or (k >= 97 and k <=109) then
			rot = rot..string.char(k+13)
		elseif (k >= 78 and k <= 90) or (k >= 110 and k <= 122) then
			rot = rot..string.char(k-13)
		else
			rot = rot..string.char(k)
		end
	end
	return rot
end
return librot
