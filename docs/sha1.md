# libenc
## How to use sha1.lua
---
### Loading
`local sha1 = dofile('path/to/sha1.lua')`

### Hash
`local hash = sha1.digest(data)`

### HMAC
`local hmac = sha1.hmac(input, key)`
