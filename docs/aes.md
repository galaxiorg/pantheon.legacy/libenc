# libenc
## How to use aes.lua / aes.min.lua
---
### Loading
`local aes = dofile('path/to/aes.lua')`

### Modes
ECB
CBC - Default
OFB
CFB

### Key lenghts
128 - Default
192
256

### Encrypting
`local encryptedData = aes.encrypt(seed, data, keyLength, mode, salt)`

### Decrypting
`local decryptedData = aes.decrypt(seed, data, keyLength, mode, salt)`
