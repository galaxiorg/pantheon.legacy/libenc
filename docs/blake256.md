# libenc
## How to use blake256.lua
---
### Loading
`local blake256 = dofile('path/to/blake256.lua')`

### Hashing
`local hash = blake256.digest(data, salt)`
