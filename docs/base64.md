# libenc
## How to use base64.lua
---
### Loading
`local base64 = dofile('path/to/base64.lua')`

### Encode
`local encoded = base64.encode(data)`

### Decode
`local decoded = base64.decode(data)`
