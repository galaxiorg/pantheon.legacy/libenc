# Libraries in libenc

aes      - AES Encryption by SquidDev

blake256 - BLAKE-256 Hash by Anavrins

md5      - MD5 Hash by Anavrins

sha1     - SHA1 Hash by Anavrins

sha256   - SHA-256 Hash by Anavrins

rot13    - Rot13 Cipher by Yonaba (kennyledet/Algorithm-Implementations)

base64   - Base64 Encoding by Alex Kloss <alexthkloss@web.de>

rsa      - RSA Encryption by 1lann (Jason Chu)
