# libenc
## How to use rsa.lua
---
### Loading
`local rsa = dofile('path/to/rsa.lua')`

### Encrypting a string
`local encrypted = rsa.encrypt(byteSize, bits, message, publicKey)`

NOTE: If publicKey is a filepath, it will read the contents of that file and use them as a key.

### Encrypting from file
`local encrypted = rsa.encryptFile(byteSize, bits, file, publicKey)`

### Decrypting a string
`local decrypted = rsa.decrypt(byteSize, bits, message, privateKey)`

NOTE: If publicKey is a filepath, it will read the contents of that file and use them as a key.

### Decrypting a string
`local decrypted = rsa.decryptFile(byteSize, bits, file, privateKey)`
