# libenc
## How to use rot.lua
---
### Loading
`local rot = dofile('path/to/rot.lua')`

### Applying ROT13
`local rot13string = rot.rot13("Example string.")`

### Applying ROT47
`local rot47string = rot.rot47("Example string.")`
