# Changes in AESLua (from the original version)

Made to meet LGPL requirements

Applies to aes.lua and min/aes.min.lua

## List of changes
[L1122](https://github.com/ccPantheon/libenc/blob/stable/aes.lua#L1122): Added a table to export the globals in a dofile (Affects other lines)

---
# Changes in base64.lua from LuaUsers (from the original version)

Made to meet LGPL2 requirements

Applies to base64.lua

## List of changes
Removed command line support (adapted to ComputerCraft)
Removed `module` and replaced with a table return (use with dofile)
Changed functions names:
	enc to encode
	dec to decode
